import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { SliderCard } from "./SliderCard";

const testimonials = [
    {
        id: 1,
        text: "Absolutely thrilled with my purchase from Purity Plants! The variety of plants available is impressive, and the quality is outstanding. My order arrived promptly and was carefully packaged. Will definitely be coming back for more!",
        author: "John Doe",
        company: "ABC Inc."
    },
    {
        id: 2,
        text: "Purity Plants has become my go-to for all things green! The website is easy to navigate, making it a breeze to find exactly what I'm looking for. Plus, their customer service is top-notch. I had a question about caring for one of my plants, and their team was incredibly helpful and knowledgeable.",
        author: "Jane Doe",
        company: "IJK Inc."
    },
    {
        id: 3,
        text: "I've ordered from Purity Plants multiple times now, and I'm always impressed. The plants arrive healthy and thriving, thanks to their careful packaging. Plus, the prices are very reasonable for the quality you receive. Highly recommend!",
        author: "William Doe",
        company: "LMN Inc."
    }
]

export default function Testimonial() {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <div className="mb-12">
        <h2 className='text-3xl text-center font-semibold dark:text-slate-100 mb-5 section-title'>
            Pure Reviews
        </h2>
        <Slider {...settings}>
            {
                testimonials.map(testimonial => 
                    <SliderCard testimonial={testimonial}/>)
            }
        </Slider>
    </div>
    
  );
}