import { Link } from "react-router-dom";
import HeroImage from "../../../assets/hero.avif";

export const Hero = () => {
  return (
    <section className="mx-20 flex gap-6 flex-col lg:flex-row dark:text-slate-100 items-center">
      <div className="my-5">
        <h1 className="text-5xl font-bold">The Ultimate Plant Store</h1>
        <p className="text-medium my-7 px-1 dark:text-slate-300 me-2">
          In addition to bringing you close to nature, we helps you enjoy a
          premium lifestyle through plants and planters! We have got you covered
          with a wide range of captivating indoor plants, and elegant planters
          that are sure to revive your living space. We ensure to utilize the
          best quality material for all our planters, and our plants are
          homegrown.
        </p>
        <Link
          to="/products"
          type="button"
          className="text-white bg-primary-700 hover:bg-primary-900 font-medium rounded-lg text-base px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
        >
          Explore Plants
        </Link>
      </div>
      <div className="my-5 lg:max-w-xl">
        <img
          className="rounded-lg max-h-full"
          src={HeroImage}
          alt="Purity Plants Hero Section"
        />
      </div>
    </section>
  );
};