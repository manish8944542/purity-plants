import React, { useState } from 'react'

export const Accordion = ({ question, answer }) => {
    const [show, setShow] = useState(false);
    return (
        <div>
            <h2 id="accordion-flush-heading-1">
                <button
                    type="button"
                    className={show ? "flex items-center justify-between w-full py-5 font-medium rtl:text-right border-b border-gray-200 dark:border-gray-700 gap-3 bg-white dark:bg-gray-900 text-gray-900 dark:text-white" : "flex items-center justify-between w-full py-5 font-medium rtl:text-right border-b border-gray-200 dark:border-gray-700 gap-3 text-gray-500 dark:text-gray-400"}
                    data-accordion-target="#accordion-flush-body-1"
                    aria-expanded={show ? "true" : "false"}
                    aria-controls="accordion-flush-body-1"
                    onClick={() => setShow(!show)}>
                    <span className="flex items-center">
                        <svg 
                        className="w-5 h-5 me-2 shrink-0" 
                        fill="currentColor" 
                        viewBox="0 0 20 20" 
                        xmlns="http://www.w3.org/2000/svg"
                        >
                        <path 
                        fillRule="evenodd" 
                        d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" 
                        clipRule="evenodd"
                        >

                        </path>
                        </svg> 
                        { question }
                        </span>
                        { show && <svg data-accordion-icon="" className="w-3 h-3 shrink-0" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                        <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5 5 1 1 5"></path>
                    </svg>}
                        {!show && <svg data-accordion-icon className="w-3 h-3 rotate-180 shrink-0" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                        <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5 5 1 1 5" />
                    </svg>}
                    
                    
                </button>
            </h2>
            <div id="accordion-flush-body-1" className={!show ? "hidden" : ""} aria-labelledby="accordion-flush-heading-1">
                <div className="p-5 border border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                    <p className="mb-2 text-gray-500 dark:text-gray-400">{ answer }</p>
                </div>
            </div>
            

        </div>
    )
}
