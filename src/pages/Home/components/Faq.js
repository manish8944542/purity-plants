import React from 'react'
import { Accordion } from './Accordion'


const faqs = [
    {
        id: 1,
        question : 'Are Plants Good For Health?',
        answer : 'Yes, plants are beneficial for health. They can improve air quality, reduce stress, boost mood, and enhance productivity.'
    },
    {
        id: 2,
        question : 'Are Plants Safe?',
        answer : 'Generally, plants are safe, but some can be toxic if ingested. It is important to research each plants care requirements and potential toxicity, especially if you have pets or small children.'
    },
    {
        id: 3,
        question : 'Can Plants Grow In Artificial Light?',
        answer : 'Yes, many plants can grow in artificial light, especially if the light spectrum and intensity mimic natural sunlight. LED grow lights, for example, can provide the necessary light for plants to photosynthesize and thrive indoors.'
    },
    {
        id: 4,
        question : 'What Types Of Delivery Options Are Available For Plants?',
        answer : 'Delivery options for plants typically include standard shipping, expedited shipping, same-day or next-day delivery (where available), local pickup, and scheduled delivery.'
    },
]
export const Faq = () => {
    
  return (
    <section id='faq'>
        <h2 className='text-3xl text-center font-semibold dark:text-slate-100 mb-5 section-title'>
            FAQs
        </h2>
        
        <div id="accordion-flush" data-accordion="collapse">
            { faqs.map(faq => <Accordion key={faq.id} question={faq.question} answer={faq.answer}/>)}
        </div>

    </section>
  )
}
