import React, { useEffect, useState } from 'react'
import { useParams, useSearchParams } from 'react-router-dom'
import useFetch from '../../hooks/useFetch';
import { DetailProduct } from './DetailProduct';

export const ProductDetailPage = () => {
    const params = useParams();
    const {data : product, setUrl, isLoading } = useFetch();
    

    useEffect(() => {
        const productId = params.id;
        const URL = `http://localhost:8000/products/${productId}`;
        setUrl(URL);
        
        
    },[params.id]);
    


  return (
    <main>
        {isLoading }
        { product && <DetailProduct product={product} />}
    </main>
  )
}
