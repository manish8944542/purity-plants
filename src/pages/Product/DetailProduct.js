import React from 'react'
import { Rating } from '../../components/Elements/Rating';

export const DetailProduct = ({ product }) => {
    console.log(product.name)
    const imagePath = "";
    return (

        <div>
            <div className='text-center font-bold text-slate-900 dark:text-slate-200'>
                <h1 className='mt-10 mb-5 text-4xl'>{product.name}</h1>
                <p className='mb-5 text-lg'>{product.overview}</p>
            </div>


            <div className="flex">
                <div className="max-w-xl my-3">
                    <img
                        className="rounded"
                        src={`http://localhost:3000${product.poster}`}
                        alt={product.name}
                    />
                </div>
                <div className="max-w-xl my-3 mx-4">
                    <p className="text-3xl font-bold text-gray-900 dark:text-slate-200">
                        <span className='mr'>$</span>
                        <span>{product.price}</span>
                    </p>
                    <p>
                        <Rating rating={product.rating} />
                    </p>
                    <p className='my-4 select-none'>
                        <span className={`font-semibold text-emerald-600 border bg-slate-100 rounded-lg px-3 py-1 mr-2 ${product.in_stock ? "" : "text-rose-600"}`}>
                            {product.in_stock ? "INSTOCK" : "OUT OF STOCK"}
                        </span>
                        <span className='font-semibold text-blue-500 border bg-slate-100 rounded-lg px-3 py-1 mr-2'>
                            {product.size} Inches
                        </span>

                    </p>
                    <p>
                        <button className={`text-white cursor:pointer bg-primary-700 hover:bg-primary-800 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-primary-600 dark:hover:bg-primary-700 focus:outline-none 
            ${product.in_stock ? "" : "cursor-not-allowed " 
                            }`}
                            disabled={product.in_stock ? "" : "disabled"}
                        >
                            Add To Cart <i className="ml-1 bi bi-plus-lg"></i>
                        </button>
                    </p>
                    <p className="text-md text-gray-900 dark:text-slate-200 my-3">
                        {product.long_description}
                    </p>
                </div>
            </div>



        </div>
    )
}
