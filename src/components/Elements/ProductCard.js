import { Link } from "react-router-dom";
import { Rating } from "./Rating";

export const ProductCard = ({ product }) => {
  return (
    <div className="m-2 max-w-xs bg-white rounded-lg border border-slate-200 shadow-md dark:bg-slate-900 dark:border-slate-700">
      <Link to={`/products/${product.id}`} className="relative">
      {product.best_seller && <span className="absolute top-4 right-2 px-2 bg-red-900 bg-opacity-90 text-white rounded">
          Best Seller
        </span>}
        
        <img
          className="rounded-t-lg w-full h-64"
          src={`http://localhost:3000${product.poster}`}
          alt={product.name}
        />
      </Link>
      <div className="p-5">
        <Link to={`/products/${product.id}`}>
          <h5 className="mb-2 text-2xl font-bold tracking-tight text-slate-900 dark:text-slate-50">
            {product.name}
          </h5>
        </Link>
        <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
    {product.overview}
        </p>

        <div className="flex items-center my-2">
          <Rating rating={product.rating} />
        </div>

        <p className="flex justify-between items-center">
          <span className="text-2xl dark:text-gray-200">
            <span>$</span>
            <span>{product.price}</span>
          </span>
          <button
            className={`inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white bg-primary-700 rounded-lg hover:bg-primary-900`}
          >
            Add To Cart <i className="ml-1 bi bi-plus-lg"></i>
          </button>
        </p>
      </div>
    </div>
  );
};