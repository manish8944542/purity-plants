import React, { useEffect, useRef, useState } from 'react'
import { Search } from './Search';
import { DropDownMenu } from './DropDownMenu';
import { Link } from 'react-router-dom';
import DarkLogo from '../../../assets/Dark Logo.png'
import Logo from '../../../assets/Light Logo.png'

export const Header = () => {

    const [darkMode, setDarkMode] = useState(
        JSON.parse(localStorage.getItem("darkMode")) || false
    );
    const [dropdown, setDropdown] = useState(false);
    const dropdownRef = useRef(null);
    const [searchSection, setSearchSection] = useState(false);

    useEffect(() => {
        localStorage.setItem("darkMode", JSON.stringify(darkMode));

        if (darkMode) {
            document.documentElement.classList.add("dark");
        } else {
            document.documentElement.classList.remove("dark");
        }
    }, [darkMode])

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
                //Clicked outside the dropdown, close it
                setDropdown(false);
            }
        };

        //Attach the event listener
        document.addEventListener("mousedown", handleClickOutside);

        //Clean up the event listener on component unmount
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [dropdownRef]);
    return (
        <header className='mb-12'>
            <nav className="bg-primary-700 dark:bg-slate-800 border-gray-200 dark:bg-gray-900">
                <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
                    <Link to="/" className="flex items-center space-x-3 rtl:space-x-reverse">
                        <img src={darkMode ? DarkLogo : Logo} className="h-14" alt="Purity Plants Logo" />
                        <span className="sr-only">Purity Plants</span>
                    </Link>
                    <div className="flex items-center relative">
                        <span
                            className={`hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-${darkMode ? "sun-fill" : "moon-fill"
                                }`}

                            onClick={() => setDarkMode(!darkMode)}>

                        </span>
                        <span onClick={() => setSearchSection(!searchSection)} className='hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-search'></span>
                        <span className='hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-cart'></span>
                        <span
                            onClick={() => setDropdown(!dropdown)}
                            className='hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-person-circle'
                        ></span>
                        {dropdown && <DropDownMenu setDropdown={setDropdown} refProp={dropdownRef} />}
                    </div>
                </div>
            </nav>
            {searchSection && <Search setSearchSection={setSearchSection} />}

        </header>
    )
}
