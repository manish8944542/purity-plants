import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { HomePage, PageNotFound, ProductDetailPage, ProductPage } from '../pages'

export const AppRoutes = () => {
  return (
    <>
        <Routes>
            <Route path='/' element={<HomePage />} title="Purity Plants"/>
            <Route path='/products' element={<ProductPage title="Products | Purity Plants"/>}/>
            <Route path='/products/:id' element={ <ProductDetailPage />}/>
            <Route path='*' element={<PageNotFound /> } />
        </Routes>
    </>
  )
}
